<%@ Control Language="C#" AutoEventWireup="false" Inherits="DotNetNuke.Modules.Admin.Authentication.Fortuitas.Settings" CodeFile="Settings.ascx.cs" %>
<%@ Register TagName="label" TagPrefix="dnn" Src="~/controls/labelcontrol.ascx" %>

<div class="dnnForm">
    <fieldset>
        <div class="dnnFormItem">
            <dnn:Label ID="lblEnabled" runat="server" /> 
            <asp:CheckBox ID="chkEnabled" runat="server" />
        </div>
        <div class="dnnFormItem">
            <dnn:Label ID="lblUseCaptcha" runat="server" /> 
            <asp:CheckBox ID="chkUseCaptcha" runat="server" />
        </div>
    </fieldset>
</div>