using DotNetNuke.Entities.Portals;
using DotNetNuke.Services.Authentication;
using DotNetNuke.Services.Exceptions;
using System;
using System.Web.UI;

namespace DotNetNuke.Modules.Admin.Authentication.Fortuitas
{
    public partial class Settings : AuthenticationSettingsBase
    {
        public override void UpdateSettings()
        {
            try
            {
                PortalController.UpdatePortalSetting(PortalId, "IMLoginEnabled", chkEnabled.Checked ? bool.TrueString : bool.FalseString);
                PortalController.UpdatePortalSetting(PortalId, "IMLoginUseCaptcha", chkUseCaptcha.Checked ? bool.TrueString : bool.FalseString);
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            try
            {
                if (Page.IsPostBack == false)
                {
                    chkEnabled.Checked = PortalController.GetPortalSettingAsBoolean("IMLoginEnabled", PortalId, false);
                    chkUseCaptcha.Checked = PortalController.GetPortalSettingAsBoolean("IMLoginUseCaptcha", PortalId, false);
                }
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }
        }
    }
}