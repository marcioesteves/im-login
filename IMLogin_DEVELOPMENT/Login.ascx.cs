#region Copyright
// 
// DotNetNukeŽ - http://www.dotnetnuke.com
// Copyright (c) 2002-2014
// by DotNetNuke Corporation
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions 
// of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
// DEALINGS IN THE SOFTWARE.
#endregion
#region Usings

using System;
using System.Linq;
using System.Net;
using System.Web;

using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Host;
using DotNetNuke.Entities.Portals;
using DotNetNuke.Entities.Users;
using DotNetNuke.Instrumentation;
using DotNetNuke.Security;
using DotNetNuke.Security.Membership;
using DotNetNuke.Services.Authentication;
using DotNetNuke.Services.Localization;
using DotNetNuke.UI.Skins;
using DotNetNuke.UI.Skins.Controls;
using DotNetNuke.UI.Utilities;

using Globals = DotNetNuke.Common.Globals;
using System.Data;
using DotNetNuke.Data;
using System.DirectoryServices.AccountManagement;

#endregion

namespace DotNetNuke.Modules.Admin.Authentication.Fortuitas
{

	/// <summary>
	/// The Login AuthenticationLoginBase is used to provide a login for a registered user
	/// portal.
	/// </summary>
	/// <remarks>
	/// </remarks>
	/// <history>
	/// 	[cnurse]	9/24/2004	Updated to reflect design changes for Help, 508 support
	///                       and localisation
	///     [cnurse]    08/07/2007  Ported to new Authentication Framework
	/// </history>
    public partial class Login : AuthenticationLoginBase
    {
        private static readonly ILog Logger = LoggerSource.Instance.GetLogger(typeof(Login));

        #region Protected Properties

        /// <summary>
        /// Gets whether the Captcha control is used to validate the login
        /// </summary>
        /// <history>
        /// 	[cnurse]	03/17/2006  Created
        ///     [cnurse]    07/03/2007  Moved from Sign.ascx.vb
        /// </history>
        protected bool UseCaptcha
        {
            get
            {
                return PortalController.GetPortalSettingAsBoolean("IMLoginUseCaptcha", PortalId, false);
                //- AuthenticationConfig.GetConfig(PortalId).UseCaptcha;
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Check if the Auth System is Enabled (for the Portal)
        /// </summary>
        /// <remarks></remarks>
        /// <history>
        /// 	[cnurse]	07/04/2007	Created
        /// </history>
        public override bool Enabled
        {
            get
            {
                return PortalController.GetPortalSettingAsBoolean("IMLoginEnabled", PortalId, false);
                // AuthenticationConfig.GetConfig(PortalId).Enabled;
            }
        }

        protected new string RedirectURL
        {
            get
            {
                string _RedirectURL = "";

                object setting = GetSetting(PortalId, "Redirect_AfterRegistration");

                if (Convert.ToInt32(setting) > 0) //redirect to after registration page
                {
                    _RedirectURL = Globals.NavigateURL(Convert.ToInt32(setting));
                }
                else
                {

                    if (Convert.ToInt32(setting) <= 0)
                    {
                        if (Request.QueryString["returnurl"] != null)
                        {
                            //return to the url passed to register
                            _RedirectURL = HttpUtility.UrlDecode(Request.QueryString["returnurl"]);
                            //redirect url should never contain a protocol ( if it does, it is likely a cross-site request forgery attempt )
                            if (_RedirectURL.Contains("://") &&
                                !_RedirectURL.StartsWith(Globals.AddHTTP(PortalSettings.PortalAlias.HTTPAlias),
                                    StringComparison.InvariantCultureIgnoreCase))
                            {
                                _RedirectURL = "";
                            }
                            if (_RedirectURL.Contains("?returnurl"))
                            {
                                string baseURL = _RedirectURL.Substring(0,
                                    _RedirectURL.IndexOf("?returnurl", StringComparison.Ordinal));
                                string returnURL =
                                    _RedirectURL.Substring(_RedirectURL.IndexOf("?returnurl", StringComparison.Ordinal) + 11);

                                _RedirectURL = string.Concat(baseURL, "?returnurl", HttpUtility.UrlEncode(returnURL));
                            }
                        }
                        if (String.IsNullOrEmpty(_RedirectURL))
                        {
                            //redirect to current page 
                            _RedirectURL = Globals.NavigateURL();
                        }
                    }
                    else //redirect to after registration page
                    {
                        _RedirectURL = Globals.NavigateURL(Convert.ToInt32(setting));
                    }
                }

                return _RedirectURL;
            }

        }

        #endregion

        #region Event Handlers

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            cmdLogin.Click += OnLoginClick;

            cmdCancel.Click += OnCancelClick;

            ClientAPI.RegisterKeyCapture(Parent, cmdLogin, 13);

            if (PortalSettings.UserRegistration == (int)Globals.PortalRegistrationType.NoRegistration)
            {
                liRegister.Visible = false;
            }
            lblLogin.Text = Localization.GetSystemMessage(PortalSettings, "MESSAGE_LOGIN_INSTRUCTIONS");

            var returnUrl = Globals.NavigateURL();
            string url;
            if (PortalSettings.UserRegistration != (int)Globals.PortalRegistrationType.NoRegistration)
            {
                if (!string.IsNullOrEmpty(UrlUtils.ValidReturnUrl(Request.QueryString["returnurl"])))
                {
                    returnUrl = Request.QueryString["returnurl"];
                }
                returnUrl = HttpUtility.UrlEncode(returnUrl);

                url = Globals.RegisterURL(returnUrl, Null.NullString);
                registerLink.NavigateUrl = url;
                if (PortalSettings.EnablePopUps && PortalSettings.RegisterTabId == Null.NullInteger
                    && !HasSocialAuthenticationEnabled())
                {
                    registerLink.Attributes.Add("onclick", "return " + UrlUtils.PopUpUrl(url, this, PortalSettings, true, false, 600, 950));
                }
            }
            else
            {
                registerLink.Visible = false;
            }

            //see if the portal supports persistant cookies
            chkCookie.Visible = Host.RememberCheckbox;

            url = Globals.NavigateURL("SendPassword", "returnurl=" + returnUrl);
            passwordLink.NavigateUrl = url;
            if (PortalSettings.EnablePopUps)
            {
                passwordLink.Attributes.Add("onclick", "return " + UrlUtils.PopUpUrl(url, this, PortalSettings, true, false, 300, 650));
            }


            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["verificationcode"]) && PortalSettings.UserRegistration == (int)Globals.PortalRegistrationType.VerifiedRegistration)
                {
                    if (Request.IsAuthenticated)
                    {
                        Controls.Clear();
                    }

                    var verificationCode = Request.QueryString["verificationcode"];


                    try
                    {
                        UserController.VerifyUser(verificationCode.Replace(".", "+").Replace("-", "/").Replace("_", "="));

                        var redirectTabId = Convert.ToInt32(GetSetting(PortalId, "Redirect_AfterRegistration"));

                        if (Request.IsAuthenticated)
                        {
                            Response.Redirect(Globals.NavigateURL(redirectTabId > 0 ? redirectTabId : PortalSettings.HomeTabId, string.Empty, "VerificationSuccess=true"), true);
                        }
                        else
                        {
                            if (redirectTabId > 0)
                            {
                                var redirectUrl = Globals.NavigateURL(redirectTabId, string.Empty, "VerificationSuccess=true");
                                redirectUrl = redirectUrl.Replace(Globals.AddHTTP(PortalSettings.PortalAlias.HTTPAlias), string.Empty);
                                Response.Cookies.Add(new HttpCookie("returnurl", redirectUrl));
                            }

                            DotNetNuke.UI.Skins.Skin.AddModuleMessage(this, Localization.GetString("VerificationSuccess", LocalResourceFile), ModuleMessage.ModuleMessageType.GreenSuccess);
                        }
                    }
                    catch (UserAlreadyVerifiedException)
                    {
                        DotNetNuke.UI.Skins.Skin.AddModuleMessage(this, Localization.GetString("UserAlreadyVerified", LocalResourceFile), ModuleMessage.ModuleMessageType.YellowWarning);
                    }
                    catch (InvalidVerificationCodeException)
                    {
                        DotNetNuke.UI.Skins.Skin.AddModuleMessage(this, Localization.GetString("InvalidVerificationCode", LocalResourceFile), ModuleMessage.ModuleMessageType.RedError);
                    }
                    catch (UserDoesNotExistException)
                    {
                        DotNetNuke.UI.Skins.Skin.AddModuleMessage(this, Localization.GetString("UserDoesNotExist", LocalResourceFile), ModuleMessage.ModuleMessageType.RedError);
                    }
                    catch (Exception)
                    {
                        DotNetNuke.UI.Skins.Skin.AddModuleMessage(this, Localization.GetString("InvalidVerificationCode", LocalResourceFile), ModuleMessage.ModuleMessageType.RedError);
                    }
                }
            }

            if (!Request.IsAuthenticated)
            {
                if (!Page.IsPostBack)
                {
                    try
                    {
                        if (Request.QueryString["username"] != null)
                        {
                            txtUsername.Text = Request.QueryString["username"];
                        }
                    }
                    catch (Exception ex)
                    {
                        //control not there 
                        Logger.Error(ex);
                    }
                }
                try
                {
                    Globals.SetFormFocus(string.IsNullOrEmpty(txtUsername.Text) ? txtUsername : txtPassword);
                }
                catch (Exception ex)
                {
                    //Not sure why this Try/Catch may be necessary, logic was there in old setFormFocus location stating the following
                    //control not there or error setting focus
                    Logger.Error(ex);
                }
            }

            var registrationType = PortalController.GetPortalSettingAsInteger("Registration_RegistrationFormType", PortalId, 0);
            bool useEmailAsUserName;
            if (registrationType == 0)
            {
                useEmailAsUserName = PortalController.GetPortalSettingAsBoolean("Registration_UseEmailAsUserName", PortalId, false);
            }
            else
            {
                var registrationFields = PortalController.GetPortalSetting("Registration_RegistrationFields", PortalId, String.Empty);
                useEmailAsUserName = !registrationFields.Contains("Username");
            }

            //-plUsername.Text = LocalizeString(useEmailAsUserName ? "Email" : "Username");
            divCaptcha1.Visible = UseCaptcha;
            divCaptcha2.Visible = UseCaptcha;
        }


        private void OnLoginClick(object sender, EventArgs e)
        {
            if ((UseCaptcha && ctlCaptcha.IsValid) || !UseCaptcha)
            {
                var loginStatus = UserLoginStatus.LOGIN_FAILURE;
                var authenticated = Null.NullBoolean;
                var message = Null.NullString;
                UserInfo objUser = null;
                string userName = null;
                string authenticationType = "Active Directory";

                // [Marcio Esteves] LDAP authentication first
                if (AuthenticateLDAP(out userName))
                    authenticated = true;
                else
                {
                    // [Marcio Esteves] IM customization - DNN authentication - check if Email should be converted to username
                    string imUserName = GetDNNUserName();
                    if (imUserName != null)
                    {
                        userName = new PortalSecurity().InputFilter(imUserName,
                                        PortalSecurity.FilterFlag.NoScripting |
                                        PortalSecurity.FilterFlag.NoAngleBrackets |
                                        PortalSecurity.FilterFlag.NoMarkup);

                        objUser = UserController.ValidateUser(PortalId, userName, txtPassword.Text, "DNN", string.Empty, PortalSettings.PortalName, IPAddress, ref loginStatus);
                        if (loginStatus == UserLoginStatus.LOGIN_USERNOTAPPROVED)
                        {
                            message = "UserNotAuthorized";
                        }
                        else
                        {
                            authenticated = (loginStatus != UserLoginStatus.LOGIN_FAILURE);
                            if (authenticated)
                                authenticationType = "DNN";
                        }
                    }
                    else
                    {
                        authenticationType = Null.NullString;
                        userName = txtUsername.Text;
                        message = string.Format("User {0} not found", txtUsername.Text);
                    }
                }

                //Raise UserAuthenticated Event
                var eventArgs = new UserAuthenticatedEventArgs(objUser, userName, loginStatus, authenticationType)
                {
                    Authenticated = authenticated,
                    Message = message,
                    RememberMe = chkCookie.Checked
                };
                OnUserAuthenticated(eventArgs);
            }
        }

        private void OnCancelClick(object sender, EventArgs e)
        {
            Response.Redirect(RedirectURL, true);
        }

        private bool HasSocialAuthenticationEnabled()
        {
            return (from a in AuthenticationController.GetEnabledAuthenticationServices()
                    let enabled = (a.AuthenticationType == "Facebook"
                                     || a.AuthenticationType == "Google"
                                     || a.AuthenticationType == "Live"
                                     || a.AuthenticationType == "Twitter")
                                  ? PortalController.GetPortalSettingAsBoolean(a.AuthenticationType + "_Enabled", PortalSettings.PortalId, false)
                                  : !string.IsNullOrEmpty(a.LoginControlSrc) && (LoadControl("~/" + a.LoginControlSrc) as AuthenticationLoginBase).Enabled
                    where a.AuthenticationType != "DNN" && enabled
                    select a).Any();
        }

        #endregion

        #region Ingram Micro

        private bool GetProperty(string name, bool defaultValue)
        {
            return Settings[name] == null ? defaultValue : string.IsNullOrWhiteSpace(Settings[name].ToString()) ? defaultValue : (bool)Settings[name];
        }

        private string GetDNNUserName()
        {
            // Check if user name exists
            UserInfo ui = DotNetNuke.Entities.Users.UserController.GetUserByName(this.PortalId, txtUsername.Text);
            if (ui != null)
                return txtUsername.Text;

            // Now check the email using the IM proxy table
            string userName = EmailToUserName(txtUsername.Text);
            if (userName != null)
                return userName;

            //-Skin.AddModuleMessage(this, string.Format("User {0} not found in DNN", txtUsername.TemplateControl), ModuleMessage.ModuleMessageType.RedError);

            return null;
        }

        public static bool TryADAuthentication(string user, string password)
        {
            // Split the user name in case a domain name was specified as DOMAIN\USER
            string[] NamesArray = user.Split(new char[] { '\\' }, 2);

            // Default vars for names & principal context type
            string DomainName = string.Empty;
            string UserName = string.Empty;
            ContextType TypeValue = ContextType.Domain;

            // Domain name was supplied
            if (NamesArray.Length > 1)
            {
                DomainName = NamesArray[0];
                UserName = NamesArray[1];
            }
            else
            {
                // Pull domain name from environment
                DomainName = Environment.UserDomainName;
                UserName = user;

                // Check this against the machine name to pick up on a workgroup
                if (string.Compare(DomainName, System.Environment.MachineName, StringComparison.InvariantCultureIgnoreCase) == 0)
                {
                    // Use the domain name as machine name (local user)
                    TypeValue = ContextType.Machine;
                }

                // DOUBLE CHECK: This will error out if computer is not part of a domain
                if (TypeValue != ContextType.Machine)
                {
                    try
                    {
                        System.DirectoryServices.ActiveDirectory.Domain.GetComputerDomain();
                    }
                    catch
                    {
                        DomainName = System.Environment.MachineName;
                        TypeValue = ContextType.Machine;
                    }
                }
            }

            // Create the temp context
            using (PrincipalContext ContextObject = new PrincipalContext(TypeValue, DomainName))
            {
                // Validate the credentials
                return ContextObject.ValidateCredentials(UserName, password);
            }
        }

        private bool AuthenticateLDAP(out string userName)
        {
            userName = txtUsername.Text;
            
            if (TryADAuthentication(userName, txtPassword.Text))
                    return true;
            else
            {
                userName = EmailToUserName(txtUsername.Text);
                if (userName != null)
                    if (TryADAuthentication(userName, txtPassword.Text))
                        return true;
            }
            return false;
        }

        private string EmailToUserName(string email)
        {
            IDataReader reader = SqlDataProvider.Instance().ExecuteReader("IMFindUser", txtUsername.Text);
            if (reader.Read())
                return reader["CAPSID"].ToString();
            return null;
        }

        #endregion
    }
}